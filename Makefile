.PHONY: all
all: build

build: $(wildcard src/*.coffee)
	coffee -l -o build -c src

.PHONY: watch
watch:
	coffee -w -l -o build -c src

.PHONY: clean
clean:
	rm -rf build
