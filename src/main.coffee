requirejs.config
  baseUrl: '/build'
  paths:
    d3: '../lib/d3.v2.min'
    jquery: '../lib/jquery-1.8.3'
    underscore: '../lib/underscore'
  shim:
    'underscore':
      exports: '_'
    'jquery':
      exports: '$'

requirejs ['glines'], (glines) ->
  $(document).ready ->
    glines.init()
