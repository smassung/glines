define ['jquery', 'underscore', 'd3'], ($, _, ignore) ->

  # the main object representing the game
  # is there a better way to create an object?
  #
  Glines = {}

  # represents a shiny ball of a certain color
  #
  class Ball
    constructor: (color, cell) ->
      @color = color
      @cell = cell

  Glines.score = 0
  Glines.scoreValues =
    {0: 0, 5: 10, 6: 12, 7: 18, 8: 28, 9: 42, 10: 82, 11: 108, 12: 138, 13: 172, 14: 210}
  Glines.width = 500
  Glines.dim = 8
  Glines.colors = ["red", "orange", "blue", "green", "grey"]
  Glines.radius = 24
  Glines.curBall = null

  # draw the grid in its initial configuration
  #
  Glines.init = ->
    Glines.setScore()
    Glines.numCells = Glines.dim * Glines.dim
    Glines.balls = []
    Glines.drawGrid()
    Glines.balls.push null for i in [0...Glines.numCells]
    Glines.addBall(0) for i in [0...3]

  # make the ball take up the whole square and then
  #   return to normal size to indicate selection
  #
  Glines.shimmer = (ball) ->
    ball
      .transition()
      .attr("r", (Glines.width / Glines.dim) / 2)
      .duration(300)
      .transition()
      .delay(300)
      .attr("r", Glines.radius)
      .duration(300)

  # sets the score on the web page
  #
  Glines.setScore = ->
    text = "<strong>#{Glines.score}</strong>"
    document.getElementById("score").innerHTML = text

  # return a random color from a selection of colors
  #
  Glines.randomColor = ->
    return Glines.colors[Math.floor(Math.random() * Glines.colors.length)]

  # add a ball in a random location in the grid
  #
  Glines.addBall = (delay) ->
    idx = Glines.nextSlot()
    Glines.balls[idx] = new Ball(Glines.randomColor(), idx)
    filt = Glines.balls.filter((b) -> b != null)
    circle = Glines.grid.selectAll("circle")
      .data(filt, (d) -> d.cell)
      .enter()
      .append("circle")
      .attr("cx", (d) -> Glines.cellX(d.cell))
      .attr("cy", (d) -> Glines.cellY(d.cell))
      .attr("r", 0)
      .attr("fill", (d) -> d.color)
      .style("stroke", "black")
      .attr("stroke-opacity", 0.7)
      .on("click", (d) ->
        Glines.shimmer(d3.select(this))
        Glines.curBall = d)
      .transition()
      .delay(delay)
      .attr("r", Glines.radius)
      .duration(600)

  # flashes a cell red to indicate you can't move there
  #
  Glines.flashRed = (cell) ->
    Glines.grid.selectAll(".row")
      .selectAll(".cell")
      .filter((d) -> d.cellNum == cell.cellNum)
      .transition()
      .style("fill", "red")
      .duration(500)
      .transition()
      .delay(500)
      .style("fill", "white")
      .duration(500)

  # tries to move a ball to this location
  #
  Glines.moveTo = (cell) ->
    return if Glines.curBall == null
    return if Glines.balls[cell.cellNum] != null
    steps = Glines.bfs(Glines.curBall.cell, cell.cellNum)
    if not steps.length
      console.log "can't move from cell #{Glines.curBall.cell} to cell #{cell.cellNum}"
      Glines.flashRed(cell)
      return
    count = 0
    console.log "moving from cell #{Glines.curBall.cell} to cell #{cell.cellNum}"
    for step in steps
      d3.selectAll("circle")
        .filter((d) -> d.cell == Glines.curBall.cell)
        .transition()
        .attr("r", Glines.radius) # hack
        .attr("cx", Glines.cellX(step))
        .attr("cy", Glines.cellY(step))
        .delay(count)
        .ease("linear")
        .duration(100)
      count += 100
    # move the ball to its new location
    Glines.balls[Glines.curBall.cell] = null
    Glines.curBall.cell = cell.cellNum
    Glines.balls[Glines.curBall.cell] = Glines.curBall
    # see if any matches were made, and if so add more time to delay
    count += 600 if Glines.checkFive(count)
    Glines.addBall(count) for i in [0...3]
    Glines.checkFive(count + 600)

  # check for 5 or more from the current ball (now in its final position)
  # param delay: if animating, how long to wait before starting
  # returns how much of a delay was used to animate removing the matches
  #
  Glines.checkFive = (delay) ->
    removed =  Glines.checkRemove(delay, Glines.leftOf, Glines.rightOf)   # horizonal
    removed += Glines.checkRemove(delay, Glines.below, Glines.above)      # vertical
    removed += Glines.checkRemove(delay, Glines.leftDown, Glines.rightUp) # positive diagonal
    removed += Glines.checkRemove(delay, Glines.leftUp, Glines.rightDown) # negative diagonal
    if removed > 0
      Glines.removeBalls(delay, [Glines.curBall])
      removed += 1
      console.log "removed #{removed} balls!"
    Glines.score += Glines.scoreValues[removed]
    Glines.setScore()

  # removes balls from the grid after they've been matched
  #
  Glines.removeBalls = (delay, balls) ->
    d3.selectAll("circle")
      .filter((d) -> d in balls)
      .transition()
      .delay(delay)
      .attr("r", 0)
      .remove()
    Glines.balls[ball.cell] = null for ball in balls

  # see if 5 or more horizontally
  #
  Glines.checkRemove = (delay, dir1, dir2) ->
    toRemove = []
    ball = dir1(Glines.curBall)
    while ball != null
      toRemove.push ball
      ball = dir1(ball)
    ball = dir2(Glines.curBall)
    while ball != null
      toRemove.push ball
      ball = dir2(ball)
    if toRemove.length >= 4
      Glines.removeBalls(delay, toRemove)
      return toRemove.length
    return 0

  Glines.nullOrMatch = (ball, idx) ->
    return null if Glines.balls[idx] is null or Glines.balls[idx].color != ball.color
    return Glines.balls[idx]

  Glines.leftOf = (ball) ->
    left = ball.cell - 1
    if ball.cell % Glines.dim == 0 then return null else return Glines.nullOrMatch(ball, left)

  Glines.rightOf = (ball) ->
    right = ball.cell + 1
    if right % Glines.dim == 0 then return null else return Glines.nullOrMatch(ball, right)

  Glines.above = (ball) ->
    above = ball.cell + Glines.dim
    if above >= Glines.numCells then return null else return Glines.nullOrMatch(ball, above)

  Glines.below = (ball) ->
    below = ball.cell - Glines.dim
    if below < 0 then return null else return Glines.nullOrMatch(ball, below)

  Glines.leftDown = (ball) ->
    down = ball.cell - Glines.dim
    return null if ball.cell % Glines.dim == 0 or down < 0
    leftDown = ball.cell - 1 - Glines.dim
    return Glines.nullOrMatch(ball, leftDown)

  Glines.leftUp = (ball) ->
    up = ball.cell + Glines.dim
    return null if ball.cell % Glines.dim == 0 or up >= Glines.numCells
    leftUp = ball.cell - 1 + Glines.dim
    return Glines.nullOrMatch(ball, leftUp)

  Glines.rightDown = (ball) ->
    right = ball.cell + 1
    down = ball.cell - Glines.dim
    return null if right % Glines.dim == 0 or down < 0
    rightDown = ball.cell + 1 - Glines.dim
    return Glines.nullOrMatch(ball, rightDown)

  Glines.rightUp = (ball) ->
    right = ball.cell + 1
    up = ball.cell + Glines.dim
    return null if right % Glines.dim == 0 or up >= Glines.numCells
    rightUp = ball.cell + 1 + Glines.dim
    return Glines.nullOrMatch(ball, rightUp)

  # find a path from start to dest, remembering how you got there
  #
  Glines.bfs = (start, dest) ->
    queue = [start]
    parents = {start: undefined}
    visited = (false for b in Glines.balls) # Glines.todo remove this and just use parents?
    while true
      cur = queue.shift()
      return [] if cur == undefined
      visited[cur] = true
      break if cur == dest
      Glines.goUp(cur, visited, queue, parent)
      Glines.goRight(cur, visited, queue, parent)
      Glines.goLeft(cur, visited, queue, parent)
      Glines.goDown(cur, visited, queue, parent)
    soln = []
    cur = dest
    while cur != start
      soln.push cur
      cur = parent[cur]
    return soln.reverse()

  # move the current ball up if possible
  #
  Glines.goUp = (cur, visited, queue, parent) ->
    up = cur + Glines.dim
    if up < Glines.numCells and not visited[up] and Glines.balls[up] == null
      visited[up] = true
      parent[up] = cur
      queue.push(up)
  
  # move the current ball right if possible
  #
  Glines.goRight= (cur, visited, queue, parent) ->
    right = cur + 1
    if right % Glines.dim != 0 and not visited[right] and Glines.balls[right] == null
      visited[right] = true
      parent[right] = cur
      queue.push(right)
  
  # move the current ball left if possible
  #
  Glines.goLeft = (cur, visited, queue, parent) ->
    left = cur - 1
    if cur % Glines.dim != 0 and not visited[left] and Glines.balls[left] == null
      visited[left] = true
      parent[left] = cur
      queue.push(left)
  
  # move the current ball down if possible
  #
  Glines.goDown = (cur, visited, queue, parent) ->
    down = cur - Glines.dim
    if down >= 0 and not visited[down] and Glines.balls[down] == null
      visited[down] = true
      parent[down] = cur
      queue.push(down)
  
  # draw the grid with d3, shading cells when moused over
  # based on http://bl.ocks.org/2605010
  #
  Glines.drawGrid = ->
    Glines.gridData()
    Glines.grid = d3.select(document.getElementById("glines"))
      .append("svg")
      .attr("width", Glines.width + 40)
      .attr("height", Glines.width + 40)
      .attr("class", "chart")
    row = Glines.grid.selectAll(".row")
      .data(Glines.cells)
      .enter().append("svg:g")
      .attr("class", "row")
    col = row.selectAll(".cell")
      .data((d) -> d)
      .enter().append("svg:rect")
      .attr("class", "cell")
      .attr("x", (d) -> d.x)
      .attr("y", (d) -> d.y)
      .attr("width", (d) -> d.width )
      .attr("height", (d) -> d.height )
      .on("mouseover", () -> d3.select(this).style("fill", '#DDF'))
      .on("mouseout",  () -> d3.select(this).style("fill", '#FFF'))
      .on("click", (d) -> Glines.moveTo(d))
      .style("fill", '#FFF')
      .style("stroke", '#000')
      .style("stroke-opacity", .5)

  # convert cell number to x location
  #
  Glines.cellX = (cell) ->
    return (1 + cell % Glines.dim) * (Glines.width / Glines.dim)

  # convert cell number to y location
  #
  Glines.cellY = (cell) ->
    return (1 + Math.floor(cell / Glines.dim)) * (Glines.width / Glines.dim)

  # finds a random unoccupied slot in the grid
  # @todo handle the case where the grid is full!
  #
  Glines.nextSlot = ->
    unoccupied = []
    for i in [0...Glines.numCells]
      unoccupied.push i if Glines.balls[i] is null
    return unoccupied[Math.floor(Math.random() * unoccupied.length)]

  # create a bunch of squares that when drawn
  #   together create a grid
  #
  Glines.gridData = ->
    Glines.cells = []
    itemWidth = (Glines.width / Glines.dim)
    xpos = itemWidth / 2
    ypos = itemWidth / 2
    cellNum = 0
    for i in [0...Glines.dim]
      Glines.cells.push []
      for j in [0...Glines.dim]
        Glines.cells[i].push cell =
          {width: itemWidth, height: itemWidth, x: xpos, y: ypos, cellNum: cellNum}
        xpos += itemWidth
        cellNum += 1
      xpos = itemWidth / 2
      ypos += itemWidth

  return Glines
